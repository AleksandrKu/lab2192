<?php

class Autoload
{
	public static function loader($class_name)
	{
		$class_name . PHP_EOL;
		$class_path = str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . ".php";
		if (file_exists($class_path)) {
			require_once $class_path;
			if (class_exists($class_name)) {
				return true;
			}
		}

		return false;
	}
}