<?php
namespace helpers;  // область видимости этого класса
class Console
{
	static private $success = "SUCCESS";
	static private $failure = "FAILURE";
	static private $warning = "WARNING";
	static private $note = "NOTE";

	static public function printLine($message, $status)
	{
		$message = (string)$message;
		switch ($status) {
			case (self::$success):
				$prefix = "Success: ";
				$color = "[0;32m";
				break;
			case (self::$warning):
				$prefix = "Warning: ";
				$color = "[1;33m";
				break;
			case (self::$failure):
				$prefix = "Failue: ";
				$color = "[0;31m";
				break;
			case (self::$note):
				$color = "[0;34m";
				break;
			default:
				$prefix = "";
				$color = '[1;37m';
		}
		echo chr(27) . $color . $prefix . $message . chr(27) . "[0m" . PHP_EOL;
	}
}
