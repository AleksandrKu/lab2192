<?php
require_once (__DIR__ . '/Autoload.php');  //подключаем класс автолоад, которая будет нам подключать все методы
spl_autoload_register("Autoload::loader"); //автозагрузчик


$asus = new \app\Asus();
$asus->restart2();
$asus->start();
$asus->printParameters();
$asus->identifyUser();
echo PHP_EOL;

$lenovo = new \app\Lenovo();
$lenovo->start();
$lenovo->printParameters();
$lenovo->identifyUser();
echo PHP_EOL;

$macbook = new \app\MacBook();
$macbook->start();
$macbook->printParameters();
$macbook->identifyUser();
echo PHP_EOL;